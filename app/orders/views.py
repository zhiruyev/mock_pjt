from rest_framework.viewsets import ModelViewSet

from app.orders.models import Order, Computer
from app.orders.serializers import OrderSerializer, ComputerSerializer


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class ComputerViewSet(ModelViewSet):
    queryset = Computer.objects.all()
    serializer_class = ComputerSerializer
