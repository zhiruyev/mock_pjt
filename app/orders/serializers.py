from rest_framework.serializers import ModelSerializer

from app.orders.models import Order, Computer


class OrderSerializer(ModelSerializer):

    class Meta:
        model = Order
        fields = '__all__'


class ComputerSerializer(ModelSerializer):

    class Meta:
        model = Computer
        fields = '__all__'
