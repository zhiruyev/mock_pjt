from django.contrib import admin

from app.orders.models import Order, Computer


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'customer',
        'address',
    )


@admin.register(Computer)
class ComputerAdmin(admin.ModelAdmin):
    list_display = (
        'name',
    )
