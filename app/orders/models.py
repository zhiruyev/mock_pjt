from django.db import models
from django.utils.translation import gettext as _


class Order(models.Model):
    created = models.DateField()
    customer = models.CharField(max_length=100)
    address = models.CharField(max_length=100)

    class Meta:
        verbose_name = _("Заказ")
        verbose_name_plural = _("Заказы")

    def __str__(self):
        return f"{self.customer} {self.address}"


class Computer(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = _("Компьютер")
        verbose_name_plural = _("Компьютер")

    def __str__(self):
        return self.name
