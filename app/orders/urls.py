from rest_framework.routers import DefaultRouter

from app.orders.views import OrderViewSet, ComputerViewSet

app_name = "orders"

router = DefaultRouter()
router.register(
    "orders",
    OrderViewSet,
    basename="orders"
)
router.register(
    "computers",
    ComputerViewSet,
    basename="computers"
)

urlpatterns = router.get_urls()
